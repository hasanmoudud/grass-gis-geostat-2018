# Linear and multiple regression in GRASS GIS

To illustrate linear and multiple regression, we analyse the relationship between mean temperature, elevation and latitude.

## Linear regression

![GRASS GIS logo](../img/grass.png)Enter in a terminal window:

```bash
g.region raster=elev_v17 -p

# linear regression model
r.regression.line mapx=elev_v17 mapy=tmean.1981_2010.annual.avg

# same model, but with machine-parsable output (-g flag)
r.regression.line mapx=elev_v17 mapy=tmean.1981_2010.annual.avg -g

# same model, but with machine-parsed output (-g flag and eval shell function)
## eval is a nice trick for transferring module output into the workflow
eval `r.regression.line mapx=elev_v17 mapy=tmean.1981_2010.annual.avg -g`
echo "a (Offset): $a"
echo "b (Gain): $b"

# Apply the model using the model variables
r.mapcalc "tmean_model_linear = $a + $b * elev_v17"
# Calculate a difference map: linear model versus observed map
r.mapcalc "tmean_model_diff_linear = tmean_model_linear - tmean.1981_2010.annual.avg"
# Apply differences color palette
r.colors tmean_model_diff_linear color=differences
# visualize
## hint: CTRL-R to find previous commands by typing a part of it; ENTER to select
d.erase
d.rast tmean_model_diff_linear
d.vect map=country_boundaries type=boundary
d.legend -t -s -b -d raster=tmean_model_diff_linear title="Diffs Temp [deg C] linear regression" title_fontsize=20 font=sans fontsize=18
# univariate statistics
r.univar tmean_model_diff_linear
```

**TASK:** Does this simple linear model explain anything?

## Multiple regression

![GRASS GIS logo](../img/grass.png)Enter in a terminal window:

```bash
# Create a latitude map, setting first the pixel geometry to an existing map (a.k.a. comp region)
g.region raster=elev_v17 -p
r.mapcalc "latitude = y()"
# Multiple linear regression model
r.regression.multi mapx=elev_v17,latitude mapy=tmean.1981_2010.annual.avg
# Apply the model using the model variables
r.mapcalc "tmean_model_multi = 40.722804 + -0.004234 * elev_v17 + -0.629130 * latitude"
# Calculate a difference map
r.mapcalc "tmean_model_diff_multi = tmean_model_multi - tmean.1981_2010.annual.avg"
# Apply differences color palette
r.colors tmean_model_diff_multi color=differences

d.erase
d.rast tmean_model_diff_multi
d.vect map=country_boundaries type=boundary
d.legend -t -s -b -d raster=tmean_model_diff_multi title="Diffs Temp [deg C] multi-regression" title_fontsize=20 font=sans fontsize=18

```

<img src="../img/ecad_v17_multi_lin_regress_model.png" alt="Multiple linear regression model" width="75%">
<br><i>Fig: Multiple linear regression model</i>

Visualize the model outputs and difference maps. Please check the manual of [r.regression.line](https://grass.osgeo.org/grass74/manuals/r.regression.line.html) and [r.regression.multi](https://grass.osgeo.org/grass74/manuals/r.regression.multi.html).

## Using R within GRASS GIS

To use R functionalities within GRASS GIS refer to [here](https://grasswiki.osgeo.org/wiki/R_statistics)


*Back to [home](README.md).*

