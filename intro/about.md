# The trainer

Markus Neteler is partner and general manager at [mundialis](https://www.mundialis.de) GmbH & Co. KG, Bonn, Germany. From 2001-2015 he worked as a researcher in Italy. Markus is co-founder of OSGeo and since 1998, coordinator of the GRASS GIS development (for details, see his private [homepage](https://grassbook.org/neteler/)). 

# About GeoSTAT 2018

The [IBOT, Prague 2018 Summer School](https://geostat-course.org/2018) is the 13th in a series of summer schools organized by R and Open Source (OS) GIS developers and enthusiasts.
GEOSTAT aims at PhD students and researchers in a range of environmental and GIS sciences, especially those focusing on analyzing spatial and spatio-temporal gridded data in R and OS GIS. The main idea of GEOSTAT is to promote various aspects of statistical analysis of spatial and spatio-temporal data using Open Source / free GIS languages and tools: R, Python, GRASS GIS, QGIS, SAGA GIS and similar.

Date:  Sunday, August 19, 2018 - 16:00 to Saturday, August 25, 2018 - 21:00

Venue: [IBOT](https://geostat-course.org/2018), Prague, CZ

