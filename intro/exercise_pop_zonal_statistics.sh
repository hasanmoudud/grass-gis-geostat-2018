# EXERCISE: 
# scope: run zonal statistics on world population raster map
# speciality: this needs to be done in the raster map projection which is an equal-area projection.
#             Hence: the vector admin areas need to be projected to the raster map for calculating
#             the zonal statistics, not the pop raster map must be reprojected!
#
# by Markus Neteler, 2018 (www.mundialis.de)
#
##################


# Data source administrative vector map:
# https://www.naturalearthdata.com/downloads/10m-cultural-vectors/

# check ZIP file content remotely
ogrinfo -ro -al -so /vsizip/vsicurl/https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_0_countries.zip

## create location
grass74 -c epsg:4326 ~/grassdata/latlong_wgs84_b
g.proj -w

v.import input="/vsizip/vsicurl/https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_0_countries.zip" output="countries"

# show columns
v.info -c map=countries 

# show one country (SQL select)
v.db.select map=countries where="SOVEREIGNT = 'Ghana'"

# extract one country
v.extract input=countries output=ghana where="SOVEREIGNT = 'Ghana'"

# roughly zoom to Ghana
g.region vector=ghana -a -p

exit

###### raster and zonal statistics part

# human settlement data, https://ghsl.jrc.ec.europa.eu/ghs_pop.php
# --> http://cidportal.jrc.ec.europa.eu/ftp/jrc-opendata/GHSL/GHS_POP_GPW4_GLOBE_R2015A/
#     --> http://cidportal.jrc.ec.europa.eu/ftp/jrc-opendata/GHSL/GHS_POP_GPW4_GLOBE_R2015A/GHS_POP_GPW42015_GLOBE_R2015A_54009_250/V1-0/GHS_POP_GPW42015_GLOBE_R2015A_54009_250_v1_0.zip  (size: 1GB)
# MN has converted the JRC GeoTIFF into a cloud optimized geotiff file and stored on his server:

gdalinfo /vsicurl/https://data.neteler.org/tmp/GHS_POP_GPW42015_GLOBE_R2015A_54009_250_v1_0_cloud_optimized.tif

# create location from (remote) GeoTIFF, World Mollweide projection = area conformal
grass74 -c /vsicurl/https://data.neteler.org/tmp/GHS_POP_GPW42015_GLOBE_R2015A_54009_250_v1_0_cloud_optimized.tif ~/grassdata/worldpop_mollweide_pop_jrc/
g.proj -w

v.proj location=latlong_wgs84_b mapset=PERMANENT input=ghana

# register Cloud Optimized GeoTIFF file as external map (instead of full import)
r.external input="/vsicurl/https://data.neteler.org/tmp/GHS_POP_GPW42015_GLOBE_R2015A_54009_250_v1_0_cloud_optimized.tif" output=GHS_POP_GPW42015_GLOBE_R2015A_250m

# computational region to Ghana, but with pixel geometry aligned to raster map for zonal statistics
g.region vector=ghana align=GHS_POP_GPW42015_GLOBE_R2015A_250m -p

v.rast.stats map=ghana raster=GHS_POP_GPW42015_GLOBE_R2015A_250m column_prefix=jrc_pop
# ... this now fetches the raster map snippet live from the remote server and performs zonal stats on the polygon, uploading the result to the DB

# full table (one line only of course)
v.db.select ghana separator=comma

# selected columns of JRC Pop 2015, compare to https://en.wikipedia.org/wiki/Ghana
v.db.select ghana separator=comma columns=jrc_pop_average,jrc_pop_sum
jrc_pop_average,jrc_pop_sum
7.07599490920485,27196211.4057986

# for comparison: Wikipedia Ghana Population 2016 estimate: 28308301

# export statistics
v.db.select ghana separator=comma file=ghana_stats.csv
soffice ghana_stats.csv

exit

###### 
# back to LatLong, for map reprojection
grass74 ~/grassdata/latlong_wgs84_b/PERMANENT/

v.proj location=worldpop_mollweide_pop_jrc mapset=PERMANENT input=ghana output=ghana_pop
v.out.ogr input=ghana_pop output=ghana_pop_LL.gpkg

exit

# look at new map in QGIS
qgis ghana_pop_LL.gpkg
