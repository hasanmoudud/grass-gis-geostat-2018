# EXERCISE: generate a Köppen-Geiger climate classification using ECA&D data
#
# Markus Neteler, 2018 (www.mundialis.de)
#
## The Köppen-Geiger climate classification is one of the most widely used climate classification systems.
#
## Method overview
# 1. extract 1200 random Koeppen-Geiger points in Europe (from Chen et al. 2013) describing the climate class at that point
# 2. sample futher ECA&D variables at point positions and save to table
# 3. convert Koeppen-Geiger class names to numeric IDs
# 4. rasterize these sampling points
# 5. install and run r.learn.ml, with RandomForestClassifier
# 6. generate Koeppen-Geiger climatic raster map based on ECAD
# 7. verify the result
#
# References:
# Chen, D. and H. W. Chen, 2013: Using the Köppen classification to quantify 
#   climate variation and change: An example for 1901-2010. Environmental 
#   Development, 6, 69-79, DOI: 10.1016/j.envdev.2013.03.007
#   http://hanschen.org/koppen/
#
# Kottek, M., J. Grieser, C. Beck, B. Rudolf, and F. Rubel, 2006: World Map of
#   the Köppen-Geiger climate classification updated. Meteorol. Z., 15, 259-263.
#   DOI: 10.1127/0941-2948/2006/0130
#   http://koeppen-geiger.vu-wien.ac.at/present.htm
#   http://koeppen-geiger.vu-wien.ac.at/pdf/Paper_2006.pdf
#
######

# check Chen et al. 2013 data organization
# get CSV file from https://gitlab.com/neteler/grass-gis-geostat-2018/tree/master/intro/aux_data/
head -n 1 aux_data/koppen_1901-2010.tsv

# import as a vector point map, restricting to current (ECA&D) region with -r
g.region -p
v.in.ascii input=aux_data/koppen_1901-2010.tsv output=chen2013_koeppen_1901_2010_global sep=tab skip=1 x=1 y=2 column="east double precision, north double precision, koeppen_1901_2010 varchar(3)" -r 

# visualize
d.mon wx0
d.vect chen2013_koeppen_1901_2010_global
d.vect map=country_boundaries type=boundary

# count number of climate description points
v.info -t chen2013_koeppen_1901_2010_global

# random data extraction of ~ 10%
v.extract input=chen2013_koeppen_1901_2010_global output=chen2013_koeppen_1901_2010_global_random random=1270

# visualize
d.erase
d.vect chen2013_koeppen_1901_2010_global_random
d.vect map=country_boundaries type=boundary

# Find out the number of different classes
# using SQL, show how many different Koeppen classes exist in attribute table, do not include column header (-c)
db.select sql="SELECT DISTINCT koeppen_1901_2010 from chen2013_koeppen_1901_2010_global_random ORDER BY koeppen_1901_2010" -c

# add legend and standard Koeppen colors
# import of Koeppen legend CSV table
# extracted from http://koeppen-geiger.vu-wien.ac.at/present.htm
# get CSV file from https://gitlab.com/neteler/grass-gis-geostat-2018/tree/master/intro/aux_data/
db.in.ogr input=koeppen_geiger_legend_2017.csv output=koeppen_geiger_legend

# check existing imported tables (they are stored in SQLite in GRASS GIS)
db.tables -p
# show table content
db.select table=koeppen_geiger_legend

# next we can join this table to the attributes of the Chen 2013 point map
# join table using key columns (map: "koeppen_1901_2010"; table: "koeppen_class")
v.info -c chen2013_koeppen_1901_2010_global_random
v.db.join map=chen2013_koeppen_1901_2010_global_random column=koeppen_1901_2010 other_table=koeppen_geiger_legend other_column=koeppen_class

# verify if all rows got populated
v.db.select chen2013_koeppen_1901_2010_global_random

# apply Koeppen-Geiger color table
v.colors chen2013_koeppen_1901_2010_global_random rules=koeppen_geiger_colors.csv

# show map, using previously joined RGB color column values
d.erase
d.vect chen2013_koeppen_1901_2010_global_random
d.vect map=country_boundaries type=boundary


#### Classification of ECA&D dataset to Koeppen-Geiger Climate Map with machine learning
# Objective: use Koeppen points in classification of ECA&D data
#- random Koeppen point sampling: DONE
#- ECA&D data analysis ready: DONE
#- prepare data for RandomForestClassifier (using r.learn.ml addon), create model and predict
#- verify result


# populate table with ECA&D values at know climatic point positions
g.list raster pattern="*.1981_2010.??.*"

for map in `g.list raster pattern="*.1981_2010.??.*"` ; do
   # column name: change dots to underscore
   mapcol=`echo $map  | tr '.' '_'`
   # vector map update from raster map at sampling points
   v.what.rast map=chen2013_koeppen_1901_2010_global_random raster=$map column=$mapcol
done

# populate table also with elevation values
v.what.rast map=chen2013_koeppen_1901_2010_global_random raster=elev_v17 column=elevation

# ... more variables could be added here!

# check updated attriute table
v.info -c chen2013_koeppen_1901_2010_global_random
v.db.select chen2013_koeppen_1901_2010_global_random

## extend table to have _numeric_ Koeppen classes for easy classification
# add new integer class column
v.db.addcolumn chen2013_koeppen_1901_2010_global_random column="koeppen_num_ID integer"

# cast string ID (grid_code) to integer
v.db.update chen2013_koeppen_1901_2010_global_random column=koeppen_num_ID query_column="CAST(gridcode AS integer)"

# verify if new column was properly generated (data type, content)
v.info -c chen2013_koeppen_1901_2010_global_random
v.db.select chen2013_koeppen_1901_2010_global_random column=koeppen_num_ID

# rasterize random points since r.learn.ml expects raster input
v.to.rast input=chen2013_koeppen_1901_2010_global_random output=chen2013_koeppen_1901_2010_global_random use=attr attribute_column=koeppen_num_ID label_column=koeppen_1901_2010
r.info chen2013_koeppen_1901_2010_global_random
r.category chen2013_koeppen_1901_2010_global_random

# install GRASS GIS addon for machine learning (there are several)
# https://grass.osgeo.org/grass7/manuals/addons/r.learn.ml.html
g.extension r.learn.ml

# prepare input data colection for r.learn.ml (raster stack)
# note, we combine raster wildcard query with additionally listed elevation map in one step
g.list raster pattern="*.1981_2010.??.*"
i.group group=ecad_1981_2010 subgroup=ecad_1981_2010 input=`g.list raster pattern="*.1981_2010.??.*" sep=comma`,elev_v17

## Note 1: to remove a group, g.remove (only -f will really do it), e.g.
#g.remove type=group name=ecad_1981_2010 -f
#
## Note 2, to easily overwrite existing maps, set this environmental variable:
# export GRASS_OVERWRITE=1
##

# perform RandomForestClassifier
r.learn.ml group=ecad_1981_2010 trainingmap=chen2013_koeppen_1901_2010_global_random output=rf_classification classifier=RandomForestClassifier n_estimators=500

r.info rf_classification
# show classes of resulting map (yet "anonymous" classes)
r.category rf_classification
# transfer category labels from training map to result
r.category rf_classification raster=chen2013_koeppen_1901_2010_global_random
r.category rf_classification
# apply Koeppen-Geiger color table
r.colors rf_classification rules=koeppen_geiger_colors.csv

# visualize
d.erase
d.rast rf_classification
d.vect chen2013_koeppen_1901_2010_global_random
d.vect map=country_boundaries type=boundary
d.legend -t -n -b raster=rf_classification title="Koeppen-Geiger classes" title_fontsize=15 font=sans fontsize=12
d.text -b text="Koeppen-Geiger climate classification based on ECA&D data (1981-2010)" color=black bgcolor=229:229:229 align=cc font=sans size=7


# compare result to Kottek, M., J. Grieser, C. Beck, B. Rudolf, and F. Rubel, 2006: World Map of the Köppen-Geiger climate classification updated. Meteorol. Z., 15, 259-263. DOI: 10.1127/0941-2948/2006/0130.
# http://koeppen-geiger.vu-wien.ac.at/present.htm
# http://koeppen-geiger.vu-wien.ac.at/pdf/Paper_2006.pdf

# TASK: try with different variable sets



############## UNUSED 1
## Using R and ML for Koeppen-Geiger/ECA&D
# start R in GRASS GIS session (https://grasswiki.osgeo.org/wiki/R_statistics/rgrass7)

R

# if needed, install rgrass7 library:
# see also https://grasswiki.osgeo.org/wiki/R_statistics#Installation
install.packages("rgrass7", dependencies = TRUE)

# Load the rgrass7 library:
library(rgrass7)
# display the metadata for your location/mapset
G <- gmeta()

#install.packages("randomForest")
#library(randomForest)
install.packages("rpart")
library(rpart)

# we skip cat,east,north variables
str(koeppenpnts[,c(4:28)])

# create decision tree
tree <- rpart(koeppen_1901_2010 ~ .,data=koeppenpnts[,c(4:28)], cp=0.0)
# show decision tree
plot(tree,uniform=T,branch=.5, main="CART tree", cex=0.5)
text(tree)

#get suggestions where to prune the tree from plot 'cp' command:
plotcp(tree)

#### END OF UNUSED 1


#### UNUSED 2
# Species prediction
# https://www.grassbook.org/wp-content/uploads/neteler/shortcourse_grass2003/notes7.html

# Fetch Aedes albopictus presence from GBIF
# https://grass.osgeo.org/grass74/manuals/addons/v.in.pygbif.html

g.extension v.in.pygbif
v.in.pygbif taxa="Aedes albopictus" rank=species output=gbif -i
v.db.select Aedes_albopictus_gbif

#### END OF UNUSED 2
