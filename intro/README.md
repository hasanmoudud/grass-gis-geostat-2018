*Main page of this document: See [https://neteler.gitlab.io/grass-gis-analysis/](https://neteler.gitlab.io/grass-gis-analysis/)*

# Analysing environmental data with GRASS GIS

<center><img src="../img/ecad_photowall.png" alt="Fun with GRASS GIS" width="75%"></center>

In the session "Analysing environmental data with GRASS GIS" we will start with a few GRASS GIS basics. We then focus on analysing the ECA&D climatic data, looking at data organization, data aggregation, univariate statistics, raster map algebra, and zonal statistics. This part is followed by classification with machine learning (RandomForest Classifier) and concluded with a quick glance at linear and multiple regression in  [GRASS GIS](https://grass.osgeo.org).

* [Session overview](#session-overview)
* [A few GRASS GIS basics](#a-few-grass-gis-basics)
* [Analysing the ECAD climatic data](#analysing-the-ecad-climatic-data)
    * Data organization: First steps with an own mapset
    * Data aggregation in GRASS GIS
    * Univariate statistics in GRASS GIS
    * Raster map algebra using map calculator
    * Zonal statistics in GRASS GIS
* [Classification with machine learning in GRASS GIS](#classification-with-machine-learning-in-grass-gis)
    * Variable preparation: raster map stacking
    * RandomForest Classifier
* [Linear and multiple regression in GRASS GIS](#linear-and-multiple-regression-in-grass-gis)
* [Further useful links](#further-useful-links)
* [References](#references)

## The trainer

Markus Neteler is partner and general manager at [mundialis](https://www.mundialis.de) GmbH & Co. KG, Bonn, Germany. From 2001-2015 he worked as a researcher in Italy. Markus is co-founder of OSGeo and since 1998, coordinator of the GRASS GIS development (for details, see his private [homepage](https://grassbook.org/neteler/)).

## Session overview

In this session, we will go through the most commonly used spatial analysis techniques in GRASS GIS. The session is a hands-on with a set of exercises covering basic raster and vector processing capabilities of GRASS GIS.

- Some quick GRASS GIS basics
- Geospatial data import (aggregated ECA&D climatic data time series)
- Simple exploratory data analysis
- Zonal statistics: GRASS GIS and R with raster library
- Machine Learning analysis to classify the European climatic zones based on ECA&D climatic data 
- Linear models in GRASS GIS and R

## Software sources

You need to have GRASS GIS 7.4 or later installed.

See [here](https://data.neteler.org/geostat2018/linklist.html) for an overview (GRASS GIS, R, QGIS for different operating systems)

## Course data download

For this exercise, we will use a GeoTIFF file and the already prepared GRASS GIS location `ecad17_ll`.

- Download course ECA&D elevation GeoTIFF file [here](https://data.neteler.org/geostat2018/ecad17_geotiffs/ecad_elev_v17.zip) (or [here](https://gitlab.com/neteler/grass-gis-geostat-2018/tree/master/intro/aux_data/ecad_elev_v17.zip))
- Download the GRASS GIS location [here](https://data.neteler.org/geostat2018/ecad17_grassdata/grassdata_ecad17_ll.zip) (or [here](https://gitlab.com/neteler/grass-gis-geostat-2018/tree/master/intro/aux_data/grassdata_ecad17_ll.zip))
- Unzip the file "ecad_elev_v17.zip" into the "$HOME/geodata/" folder (create folder if needed)
- Unzip the file "grassdata_ecad17_ll.zip" into the "$HOME/grassdata/" folder (create folder if needed)

# A few GRASS GIS basics

This short section covers a few concepts you should know before continuing with the exercises.
Please read on in [01_grass-gis-basics.md](01_grass-gis-basics.md).

# Analysing the ECAD climatic data

Please read on in [02_grass-gis_ecad_analysis.md](02_grass-gis_ecad_analysis.md).

# Classification with machine learning in GRASS GIS

Please read on in [03_grass-gis_ecad_randomforest.md](03_grass-gis_ecad_randomforest.md).

# Linear and multiple regression in GRASS GIS

Please read on in [04_grass-gis_ecad_regression.md](04_grass-gis_ecad_regression.md).

## Further useful links

- GRASS GIS wiki: <https://grasswiki.osgeo.org/wiki/GRASS-Wiki>
- GRASS GIS and R for time series processing: <https://grasswiki.osgeo.org/wiki/Temporal_data_processing/GRASS_R_raster_time_series_processing>

## References

- Related books: <https://grass.osgeo.org/documentation/books/>
- Related tutorials and articles: <https://grass.osgeo.org/documentation/tutorials/>
- Neteler, M., Bowman, M.H., Landa, M. and Metz, M. (2012): GRASS GIS: a multi-purpose Open Source GIS. Environmental Modelling & Software, 31: 124-130 ([DOI](http://dx.doi.org/10.1016/j.envsoft.2011.11.014))

## Original data sources

- [ECA&D](http://www.ecad.eu/) V17 daily climatic data 1950-2017: aggregated in GRASS GIS to annual and monthly data of 30 years averages, 0.25° spatial resolution
- Related European elevation model
- [NaturalEarth](http://www.naturalearthdata.com/downloads/110m-cultural-vectors/) Admin0 country borders
- Köppen climate types for the period 1901-2010 on different time scales by [Chen et al 2013](http://hanschen.org/koppen/)

------------------------------------------------------------------------

*Last changed: 17 Aug 2018*

[GRASS GIS manual main index](https://grass.osgeo.org/grass74/manuals/) | [Topics index](https://grass.osgeo.org/grass74/manuals/topics.html) | [Keywords Index](https://grass.osgeo.org/grass74/manuals/keywords.html) | [Full index](https://grass.osgeo.org/grass74/manuals/full_index.html) | [Raster index](https://grass.osgeo.org/grass74/manuals/raster.html) | [Vector index](https://grass.osgeo.org/grass74/manuals/vector.html) | [Temporal index](https://grass.osgeo.org/grass74/manuals/temporal.html)

[![Creative Commons License](../img/ccbysa.png)](http://creativecommons.org/licenses/by-sa/4.0/) "Analysing environmental data with GRASS GIS" by [Markus Neteler](https://www.mundialis.de/neteler/), 2018

Tutorial based on [Hands-on to GIS and RS with GRASS GIS](http://scientificpubs.com/grass_workshop_itc/guided_tour.html) by Veronica Andreo, Sajid Pareeth and Paulo van Breugel (2017) which was adapted from [Geostat 2015](https://geostat-course.org/) exercises prepared by [Markus Neteler](https://grassbook.org/neteler/).

*[About](about.md)*

