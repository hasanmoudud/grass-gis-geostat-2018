# EXERCISE: 
# - Download and process SRTM data, calculate watersheds and accumulation map
# - download and import Prague admin4 boundary map from OSM
# - download Sentinel-2 data and calculate NDVI
#
# Extras: 
#   - Cloud Optimized GeoTIFF is used
#   - GDAL's VSI driver helps to download and import in one step
#
# by Markus Neteler, 2018 (www.mundialis.de)
#
##################
# Data source administrative vector map:
# https://www.naturalearthdata.com/downloads/10m-cultural-vectors/

# check ZIP file content remotely
ogrinfo -ro -al -so /vsizip/vsicurl/https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_0_countries.zip

## create GRASS GIS location from EPSG code
grass74 -c epsg:4326 ~/grassdata/latlong_wgs84_b
g.proj -w

# download and import world country map in one step
v.import input="/vsizip/vsicurl/https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_0_countries.zip" output="countries"

# show columns
v.info -c map=countries 

# check content of attribute table
v.db.select map=countries

# show one country (SQL select)
v.db.select map=countries where="SOVEREIGNT = 'Czechia'"

# extract one country
v.extract input=countries output=czechia where="SOVEREIGNT = 'Czechia'"

# roughly zoom to Czechia
g.region vector=czechia -p

# SRTM World 30m is available as single map from https://www.datenatlas.de (86 GB)
#    https://hannes.enjoys.it/blog/2017/01/one-srtmgl1-geotiff-to-rule-them-all/
##    gdalinfo /vsicurl/https://www.datenatlas.de/geodata/public/srtmgl1/srtmgl1.003.tif
##    .. just a moment, the world at 30m is biggish file!

## reduced version, cloud optimized:
#srtmgl1_003_EUROPE_cloud_optimized.tif bbox:
# GDAL: -projwin ulx uly lrx lry
#   55N
#10W    20E
#   45N
# nice gdal_translate srtmgl1.003.tif srtmgl1_003_EUROPE_cloud_optimized_512blocks.tif -co TILED=YES -co BLOCKYSIZE=512 --config GDAL_TIFF_OVR_BLOCKSIZE 512 -co COMPRESS=LZW -co BIGTIFF=YES -projwin 10 55 20 45

# register Cloud Optimized GeoTIFF file as external map (instead of full import)
## too slow: r.external input="/vsicurl/https://data.neteler.org/tmp/srtmgl1_003_EUROPE_cloud_optimized.tif" output=srtmgl1_003_EUROPE
#
#gdalinfo /vsicurl/https://data.neteler.org/tmp/srtmgl1_003_EUROPE_cloud_optimized_512blocks.tif
#gdal_translate /vsicurl/https://data.neteler.org/tmp/srtmgl1_003_EUROPE_cloud_optimized_512blocks.tif srtmgl1_CZ.tif -co COMPRESS=LZW 
## we use the already downloaded srtmgl1_CZ.tif
r.external input=srtmgl1_CZ.tif output=srtmgl1_CZ
r.info srtmgl1_CZ
r.colors srtmgl1_CZ color=srtm_plus

# computational region to Czechia, but with pixel geometry aligned to raster map for zonal statistics
g.region vector=czechia align=srtmgl1_CZ -p

# to avoid that the DEM falls directly onto the border we expand the computational region into all directions by 3 pixels (i.e. 0:00:03)
g.region n=n+0:00:03 s=s-0:00:03 w=w-0:00:03 e=e+0:00:03 -p

d.mon wx0
# SRTM of CZ
d.rast srtmgl1_CZ
d.vect czechia type=boundary

########################
# Watersheds and accumulation
r.watershed elevation=srtmgl1_CZ threshold=10000 basin=srtmgl1_CZ_basins_10k accumulation=srtmgl1_CZ_accum_10k tci=srtmgl1_CZ_tci_10k
d.rast srtmgl1_CZ_basins_10k
d.rast srtmgl1_CZ_accum_10k
d.rast srtmgl1_CZ_tci_10k

# shaded terrain map
r.relief srtmgl1_CZ output=srtmgl1_CZ_shaded
d.shade srtmgl1_CZ_shaded color=srtmgl1_CZ_basins_10k
# For comparison: WISE Large rivers and large lakes
# https://www.eea.europa.eu/data-and-maps/data/wise-large-rivers-and-large-lakes

########################
# Prague admin4

# to get Prague admin4 areas from OSM, download via https://wambachers-osm.website/boundaries/ (requires OSM login)
v.import input=/home/mneteler/data/cz_prague/Prague_AL4.GeoJson output=osm_admin_prague_AL4
g.region vector=osm_admin_prague_AL4 align=srtmgl1_CZ -p
d.erase
d.shade srtmgl1_CZ_shaded color=srtmgl1_CZ_accum_10k
d.vect osm_admin_prague_AL4 type=boundary

########################
# Sentinel-2
# http://training.gismentors.eu/grass-gis-workshop-jena-2018/units/21.html

g.extension extension=i.sentinel
# get list, search for Sentinel-2 L2A data
i.sentinel.download -l settings=~/.sentinel_esa map=osm_admin_prague_AL4 area_relation=Contains start=2018-03-01 end=2018-07-31 producttype=S2MSI2Ap

# download the selected scene(s)
i.sentinel.download  settings=~/.sentinel_esa map=osm_admin_prague_AL4 area_relation=Contains start=2018-03-01 end=2018-07-31 producttype=S2MSI2Ap output=~/data/cz_prague/

# TASK: calculate NDVI with i.vi
# https://grass.osgeo.org/grass74/manuals/i.vi.html
# Sentinel-2 Bands = [ 8,  4]

exit
