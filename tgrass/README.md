# Spatio-temporal data processing and visualization with GRASS GIS

In the session [**Spatio-temporal data processing and visualization with GRASS GIS**](https://gitpitch.com/veroandreo/grass-gis-geostat-2018/master?p=tgrass&grs=gitlab) 
we will start with a few basics on the temporal framework in GRASS GIS or **TGRASS**.
Then, we will move to a hands-on tutorial in which we will first learn how to create
space-time datasets (STDS) and assign time stamps to maps. In addition we will learn 
to list maps in STDS, perform temporal operations, temporal aggregations, extract
univariate statistics and zonal statistics from time series. All along the session,
we'll see different visualization options available in 
[GRASS GIS](https://grass.osgeo.org/).

## The trainer

Veronica Andreo is a researcher for [CONICET](http://www.conicet.gov.ar/?lan=en)
working at the Institute of Tropical Medicine [(INMeT)](https://www.argentina.gob.ar/salud/inmet)
in Puerto Iguazú, Argentina. Her main interests are remote sensing and GIS tools
for disease ecology research fields and applications. 
Vero is an [OSGeo](http://www.osgeo.org/) Charter member and a [FOSS4G](http://foss4g.org/) 
enthusiast and advocate. 
She is part of the [GRASS GIS Development team](https://grass.osgeo.org/home/credits/) 
and she also teaches introductory and advanced courses and workshops, especially 
on GRASS GIS [time series modules](https://grasswiki.osgeo.org/wiki/Temporal_data_processing)
and their applications.

## Session overview

In this session, we will go through the most commonly used spatial and temporal 
analysis techniques in GRASS GIS. The session is a hands-on tutorial covering 
the most important temporal and visual capabilities of GRASS GIS.

![tgrass-collage](../img/tgrass_collage.png)

- TGRASS basic concepts and notions
- TGRASS modules and workflow overview
- Hands-on:
    - Creation of time series
    - Map registration
    - Lists
    - Temporal algebra
    - Univariate statistics of time series
    - Temporal aggregation: full time series and with granularity
    - Zonal statistics of time series
    - Different visualizations: timeline, time series plot, frames, mapswipe, animations.

## Software sources

- GRASS GIS 7.4. See [here](https://gitlab.com/veroandreo/grass-gis-geostat-2018/blob/master/README.md) for an overview for different operating systems.
- [Python Lex-Yacc (PLY)](http://www.dabeaz.com/ply/) library is required by the [temporal algebra module](https://grass.osgeo.org/grass74/manuals/t.rast.algebra.html). See [here](https://grass.osgeo.org/grass74/manuals/t.rast.algebra.html#references) for instructions to install in the most common Linux distributions. If GRASS GIS is installed through OSGeo4W in MS Windows, PLY will come automatically as dependency. 
- [v.strds.stats](https://grass.osgeo.org/grass74/manuals/addons/v.strds.stats.html) add-on. Install within a GRASS GIS session with `g.extension extension=v.strds.stats`.

## Course data download

For this exercise, we will use the North Carolina location and `modis_lst` mapset.

- [North Carolina location (full dataset, 150Mb)](https://grass.osgeo.org/sampledata/north_carolina/nc_spm_08_grass7.zip): download and unzip within `$HOME/grassdata`. 
- [modis_lst mapset (2Mb)](https://gitlab.com/veroandreo/grass-gis-geostat-2018/blob/master/data/modis_lst.zip): download and unzip within the North Carolina location in `$HOME/grassdata/nc_spm_08_grass7`.

## Further useful links

- [Temporal data processing wiki](https://grasswiki.osgeo.org/wiki/Temporal_data_processing)
- [GRASS GIS and R for time series processing wiki](https://grasswiki.osgeo.org/wiki/Temporal_data_processing/GRASS_R_raster_time_series_processing)
- [GRASS GIS temporal workshop at NCSU](http://ncsu-geoforall-lab.github.io/grass-temporal-workshop/)
- [TGRASS workshop at FOSS4G Europe 2017](https://gitlab.com/veroandreo/tgrass_workshop_foss4g_eu)
- [GRASS GIS workshop held in Jena 2018](http://training.gismentors.eu/grass-gis-workshop-jena-2018/index.html)

## References

- Related books: <https://grass.osgeo.org/documentation/books/>
- Related tutorials and articles: <https://grass.osgeo.org/documentation/tutorials/>
- Neteler, M., Bowman, M.H., Landa, M. and Metz, M. (2012): *GRASS GIS: a multi-purpose Open Source GIS*. Environmental Modelling & Software, 31: 124-130. [DOI](http://dx.doi.org/10.1016/j.envsoft.2011.11.014)
- Gebbert, S., Pebesma, E. (2014). *A temporal GIS for field based environmental modeling*. Environmental Modelling & Software, 53, 1-12. [DOI](https://doi.org/10.1016/j.envsoft.2013.11.001)
- Gebbert, S., Pebesma, E. (2017). *The GRASS GIS temporal framework*. International Journal of Geographical Information Science 31, 1273-1292. [DOI](http://dx.doi.org/10.1080/13658816.2017.1306862)

------------------------------------------------------------------------

*Last changed: 21 Aug 2018*

[![Creative Commons License](../img/ccbysa.png)](http://creativecommons.org/licenses/by-sa/4.0/) "Spatio-temporal data processing and visualization with GRASS GIS" by [Veronica Andreo](https://veroandreo.wordpress.com/), 2018

[GRASS GIS manual main index](https://grass.osgeo.org/grass74/manuals/) | [Topics index](https://grass.osgeo.org/grass74/manuals/topics.html) | [Keywords Index](https://grass.osgeo.org/grass74/manuals/keywords.html) | [Full index](https://grass.osgeo.org/grass74/manuals/full_index.html) | [Raster index](https://grass.osgeo.org/grass74/manuals/raster.html) | [Vector index](https://grass.osgeo.org/grass74/manuals/vector.html) | [Temporal index](https://grass.osgeo.org/grass74/manuals/temporal.html)
